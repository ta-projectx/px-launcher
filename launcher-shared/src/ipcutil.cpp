#include "ipcutil.h"

std::string ipcshared::get_ipc_server_addr(px::xutil::XDisplayComponent &display)  {
    return ipcshared::ipc_basedir + display.to_string() + ".daemon";
}

std::string ipcshared::get_ipc_reply_addr(uint16_t reply_id) {
    return ipc_basedir + std::to_string(reply_id) + ".reply";
}