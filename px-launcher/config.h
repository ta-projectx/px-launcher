#pragma once

#include <set>
#include <map>
#include <vector>
#include <cstdint>
#include <string>
#include <memory>
#include "shared/xcomponent.h"
#include <xcb/xcb.h>

enum class ScalingAlgorithm : uint8_t { NEAREST, BILINEAR, RAA };
enum class FrameRateMode : uint8_t { LOW, NORMAL, HIGH };
enum class MatchMode : uint8_t { ABSPATH, EXECNAME };

enum class XSettingItemType : uint8_t {NONE = 255, INTEGER = 0, STRING = 1, COLOR = 2};

struct MatchGroupSetting {
    ScalingAlgorithm scalealg = ScalingAlgorithm::NEAREST;
    FrameRateMode framerate = FrameRateMode::NORMAL;
};

struct MatchGroup {
    MatchGroupSetting setting;
    //null if not assigned yet
    std::shared_ptr<px::xutil::XDisplayComponent> assigned_dpy = nullptr;
};

struct XSettingItem {
    XSettingItemType type = XSettingItemType::NONE;
    int32_t int_value;
    std::string str_value;
    struct Color{
        uint16_t r;
        uint16_t b;
        uint16_t g;
        uint16_t a;
    } color_value;
};

struct Config {
    bool is_complete = false;
    /**
     * Contains all available groups in the config.
     */
    std::vector<MatchGroup> groups;
    std::map<std::string,XSettingItem> xsettings;

    // parent_level -> string_to_match -> group
    std::vector<std::map<std::string,uint32_t>> execname_match;
    std::vector<std::map<std::string,uint32_t>> abspath_match;
    int8_t max_parent_level = -1;
};

class ConfigManager {
public:
    static std::string default_config_file();
    static Config from_file(std::string path);
    static void populate_res_from_randr(xcb_connection_t* fconn, Config& cfg);
};