#include "randr.h"
#include <glog/logging.h>
#include <xcb/randr.h>
#include <xcb/xcb_util.h>
#include "shared/memory.h"
#include <mutex>

using namespace px::memory;
using namespace subdaemon;
using namespace std;

class RandRSetting::Priv {
public:
    mutex global_mutex;
    xcb_connection_t* conn;
    xcb_window_t root;
    bool init_success = false;

    //used if this hasn't been initialized (updated) yet
    bool has_queue = false;
    pair<uint16_t, uint16_t> queued_sz;

    xcb_randr_mode_t new_mode(uint16_t w, uint16_t h);
    bool resize_screen(uint16_t w, uint16_t h);
    void resize_display_stub(pair<uint16_t, uint16_t> sz);
};

RandRSetting::RandRSetting() {
    p = make_unique<Priv>();
}

RandRSetting::~RandRSetting() {}

void RandRSetting::update(xcb_connection_t *conn, xcb_screen_t *scr) {
    unique_lock<mutex> lock {p->global_mutex};

    p->conn = conn;
    p->root = scr->root;
    //init RANDR
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    pair<uint16_t,uint16_t> rr_req_ver{1,2};
    auto ver = make_unique_malloc(xcb_randr_query_version_reply(conn, xcb_randr_query_version(conn, rr_req_ver.first, rr_req_ver.second), &err.pop()));
    if(err.get()) {
        LOG(WARNING) << "RRQueryVersion error " << xcb_event_get_error_label(err.get()->error_code) << ". RANDR will be disabled.";
        p->init_success = false;
        return;
    }
    if(ver->major_version <= rr_req_ver.first && ver->minor_version < rr_req_ver.second) {
        LOG(WARNING) << "RANDR version lower than requested. RANDR will be disabled.";
        p->init_success = false;
        return;
    }
    //success, set the flag and run the queue if any
    p->init_success = true;
    if(p->has_queue) {
        p->has_queue = false;
        p->resize_display_stub(p->queued_sz);
    }
}

void RandRSetting::resize_display(pair<uint16_t, uint16_t> sz) {
    unique_lock<mutex> lock {p->global_mutex};

    if(p->init_success) {
        p->resize_display_stub(sz);
    } else {
        p->has_queue = true;
        p->queued_sz = sz;
    }
}

xcb_randr_mode_t RandRSetting::Priv::new_mode(uint16_t w, uint16_t h) {
    //mode name
    stringstream ss_modname;
    ss_modname << w << "x" << h;
    string modname = ss_modname.str();
    //modeinfo
    xcb_randr_mode_info_t modinfo;
    //all fields are zero ...
    memset(&modinfo, 0, sizeof(modinfo));
    //... except the following
    modinfo.htotal = modinfo.width = w;
    modinfo.vtotal = modinfo.height = h;
    modinfo.name_len = modname.size();
    //request
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto rep = make_unique_malloc(xcb_randr_create_mode_reply(conn, xcb_randr_create_mode(conn, root, modinfo, modname.size(), modname.c_str()), &err.pop()));
    if(err.get()) {
        LOG(WARNING) << "RRCreateMode error: " << xcb_event_get_error_label(err.get()->error_code);
        return 0;
    }
    //we only need the mode ID
    return rep->mode;
}

bool RandRSetting::Priv::resize_screen(uint16_t w, uint16_t h) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    //based on 96 dpi
    constexpr float px_per_mm = 96.0/25.4;
    err.pop() = xcb_request_check(conn, xcb_randr_set_screen_size_checked(conn, root, w, h, w / px_per_mm, h / px_per_mm));
    if(err.get()) {
        LOG(WARNING) << "RRSetScreenSize error: " << xcb_event_get_error_label(err.get()->error_code);
        return false;
    }
    return true;
}

void RandRSetting::Priv::resize_display_stub(pair<uint16_t, uint16_t> sz) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;

    //for Xvfb, this is predefined as "screen"
    static const string target_output_name = "screen";

    xcb_randr_output_t target_output = 0;
    xcb_randr_crtc_t target_crtc;
    xcb_randr_mode_t target_mode = 0;

    {
        auto rep = make_unique_malloc(xcb_randr_get_screen_resources_reply(conn, xcb_randr_get_screen_resources(conn, root), &err.pop()));
        if(err.get()) {
            LOG(WARNING) << "RRGetScreenResourcesCurrent error: " << xcb_event_get_error_label(err.get()->error_code);
            return;
        }
        //output and CRTC
        auto outs = xcb_randr_get_screen_resources_outputs(rep.get());
        for(int i = 0; i < rep->num_outputs; ++i) {
            auto out = outs[i];
            auto out_rep = make_unique_malloc(xcb_randr_get_output_info_reply(conn, xcb_randr_get_output_info(conn, out, XCB_TIME_CURRENT_TIME), &err.pop()));
            string output_name {(char*)xcb_randr_get_output_info_name(out_rep.get()), out_rep->name_len};
            if(output_name == target_output_name) {
                target_output = out;
                target_crtc = out_rep->crtc;
                break;
            }
        }
        if(!target_output) {
            LOG(WARNING) << "RANDR: Desired output not found.";
            return;
        }
        //search for existing suitable mode
        auto modes_it = xcb_randr_get_screen_resources_modes_iterator(rep.get());
        for(int i = 0; i < rep->num_modes; ++i) {
            if(modes_it.data->width == sz.first && modes_it.data->height == sz.second) {
                target_mode = modes_it.data->id;
                break;
            }
            xcb_randr_mode_info_next(&modes_it);
        }
    }

    //create new mode on the target display server
    if(!target_mode) {
        target_mode = new_mode(sz.first, sz.second);
    }
    if(!target_mode) {
        return;
    }

    //add the mode to the output
    err.pop() = xcb_request_check(conn, xcb_randr_add_output_mode_checked(conn, target_output, target_mode));
    if(err.get()) {
        LOG(WARNING) << "RRAddOutputMode error: " << xcb_event_get_error_label(err.get()->error_code);
        return;
    }

    //get last configuration timestamp on CRTC
    uint32_t max_sz = 0;
    uint16_t max_w, max_h;
    xcb_timestamp_t config_timestamp;
    {
        auto rep = make_unique_malloc(xcb_randr_get_screen_info_reply(conn, xcb_randr_get_screen_info(conn, root), &err.pop()));
        if(err.get()) {
            LOG(WARNING) << "RRGetScreenInfo error: " << xcb_event_get_error_label(err.get()->error_code);
            return;
        }
        config_timestamp = rep->config_timestamp;
        //find max sizes
        auto sizes = xcb_randr_get_screen_info_sizes(rep.get());
        for(int i = 0; i < rep->nSizes; ++i) {
            auto& size = sizes[i];
            uint32_t area = size.width * size.height;
            if(area > max_sz) {
                max_sz = area;
                max_w = size.width;
                max_h = size.height;
            }
        }
    }

    //resize screen to maximum size possible, because at anytime the screen must be larger than the CRTC.
    if(!resize_screen(max_w, max_h)) {
        return;
    }

    //set the mode at CRTC associated with the output
    make_unique_malloc(xcb_randr_set_crtc_config_reply(conn, xcb_randr_set_crtc_config(conn, target_crtc, XCB_TIME_CURRENT_TIME, config_timestamp, 0, 0, target_mode, XCB_RANDR_ROTATION_ROTATE_0, 1, &target_output), &err.pop()));
    if(err.get()) {
        LOG(WARNING) << "RRSetCrtcConfig error: " << xcb_event_get_error_label(err.get()->error_code);
        return;
    }

    //resize screen back to desired size
    if(!resize_screen(sz.first, sz.second)) {
        return;
    }
}