#include "log.h"
#include <unistd.h>
#include <string>
#include "shared/util.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/file.h>

using namespace std;
using namespace px::util;

namespace priv {
    string read_file(string filename) {
        static const string err = "(error)";
        auto fh = fopen(filename.c_str(), "r");
        if(!fh) {
            return err;
        }
        uint8_t* ret = new uint8_t[1024];
        uint16_t read = fread(ret, 1, 1024, fh);
        ret[read] = 0;
        fclose(fh);
        return (char*)ret;
    }
}

Log::Log() {
    //populate constants
    pid = getpid();
    ruid = getuid();
    euid = geteuid();
    string file_cmdline = string_printf("/proc/%d/cmdline", pid);
    cmdline = priv::read_file(file_cmdline);
}

void Log::append_log(const char *target_dpy, const std::string msg) {
    constexpr auto logfile = "/tmp/px-preload.txt";

    //timestamp
    char timestamp_str[32];
    time_t timer;
    time(&timer);
    struct tm* timeinfo = localtime(&timer);
    int ts_sz = strftime(timestamp_str, sizeof(timestamp_str), "%Y-%m-%d %H:%M:%S,", timeinfo);
    //generate line text
    const char* w_displayname = target_dpy ? target_dpy : "(NONE)";
    string lineout = string_printf("%d,%d,%d,%s,%s,%s\n", pid, ruid, euid, cmdline.c_str(), w_displayname, msg.c_str());

    //open target log file
    int fh = open(logfile, O_CREAT | O_APPEND | O_WRONLY, 0666);

    if(fh < 0) {
//        perror("Cannot open file");
        return;
    }
    //automatic file close
    DeleteInvoker fhdel([&fh]{
        close(fh);
    });
    //acquire file lock
    if(flock(fh, LOCK_EX) < 0) {
//        perror("Cannot lock file");
        return;
    }
    //append to logfile
    int wrote = 0;
    wrote += write(fh, timestamp_str, ts_sz);
    wrote += write(fh, lineout.c_str(), lineout.size());
    //release file lock
    flock(fh, LOCK_UN);
}