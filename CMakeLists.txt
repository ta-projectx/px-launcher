cmake_minimum_required(VERSION 3.4)
project(px_launcher)

set(CMAKE_CXX_STANDARD 14)

set(ADDITIONAL_COMPILE_OPTIONS ${ADDITIONAL_COMPILE_OPTIONS} -fPIC)

add_subdirectory(deps/px-shared)
include_directories(launcher-shared/inc deps/px-shared/inc)

#launcher-shared
add_library(launcher_shared STATIC
    launcher-shared/inc/ipcmsg.h
    launcher-shared/src/ipcclient.cpp
    launcher-shared/inc/ipcclient.h
    launcher-shared/inc/ipcutil.h
    launcher-shared/src/ipcutil.cpp)
target_compile_options(launcher_shared PRIVATE ${ADDITIONAL_COMPILE_OPTIONS})

find_package(PkgConfig REQUIRED)
pkg_check_modules(GDK3 gdk-3.0)
pkg_check_modules(XCB xcb)
pkg_check_modules(XCB_RANDR xcb-randr)
pkg_check_modules(XCB_UTIL xcb-util)

#px_preload
set(PX_LD_LINK_LIBRARIES px_shared
    launcher_shared
    xcb
    dl)
add_library(px_preload SHARED px-preload/preload.cpp
    px-preload/preload.h
    px-preload/log.cpp
    px-preload/log.h)
add_dependencies(px_preload px_shared launcher_shared)
target_compile_options(px_preload PRIVATE ${ADDITIONAL_COMPILE_OPTIONS})
target_link_libraries(px_preload ${PX_LD_LINK_LIBRARIES} ${ADDITIONAL_LINK_OPTIONS})

#px_launcher
set(PX_LN_LINK_LIBRARIES px_shared
    launcher_shared
    pthread
    atomic
    glog
    dl
    ${XCB_LIBRARIES}
    ${XCB_UTIL_LIBRARIES}
    ${XCB_RANDR_LIBRARIES}
    ${GDK3_LIBRARIES})
set(PX_LN_PREBUILT_DIR ${CMAKE_BINARY_DIR}/px_launcher-prebuilt)
#set(PX_LN_PREBUILT_RESOURCE ${PX_LN_PREBUILT_DIR}/resources.o)
add_executable(px_launcher #${PX_LN_PREBUILT_RESOURCE}
    px-launcher/main.cpp
    px-launcher/config.cpp
    px-launcher/config.h
    px-launcher/util.cpp
    px-launcher/util.h
    px-launcher/ipcserver.cpp
    px-launcher/ipcserver.h
    px-launcher/client.cpp
    px-launcher/client.h
    px-launcher/daemon.cpp
    px-launcher/daemon.h
    px-launcher/rdevtfd.cpp
    px-launcher/rdevtfd.h
    px-launcher/sighandler.h
    px-launcher/sighandler.cpp
    px-launcher/subdaemon/sizescalemon.cpp
    px-launcher/subdaemon/sizescalemon.h
    px-launcher/subdaemon/dpymgr.cpp
    px-launcher/subdaemon/dpymgr.h
    px-launcher/raii/process.cpp
    px-launcher/raii/process.h
    px-launcher/subdaemon/xsettingsd.cpp
    px-launcher/subdaemon/xsettingsd.h
    px-launcher/subdaemon/randr.cpp
    px-launcher/subdaemon/randr.h)
target_include_directories(px_launcher PRIVATE
    px-launcher
    ${GDK3_INCLUDE_DIRS})
#add_custom_command(OUTPUT ${PX_LN_PREBUILT_RESOURCE}
#    COMMAND echo Building prebuilt resource ${PX_LN_PREBUILT_RESOURCE}
#    COMMAND mkdir -p ${PX_LN_PREBUILT_DIR}
#    COMMAND ld -r -b binary -o ${PX_LN_PREBUILT_RESOURCE} hello.txt
#    WORKING_DIRECTORY /${CMAKE_SOURCE_DIR}/px-launcher/resources)
add_dependencies(px_launcher px_shared launcher_shared)
target_compile_options(px_launcher PRIVATE ${ADDITIONAL_COMPILE_OPTIONS})
target_link_libraries(px_launcher ${PX_LN_LINK_LIBRARIES} ${ADDITIONAL_LINK_OPTIONS})
